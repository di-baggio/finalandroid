package com.example.app;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.app.common.Constantes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PantallaRegistro extends AppCompatActivity {
    Button btnAgregar ;
    Button btnSalir ;
    EditText txtEmail;
    EditText txtPassword;
    EditText txtNombre;
    EditText txtApellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registro);
        ActionBar actionbar = getSupportActionBar();
        actionbar.hide();

        btnAgregar = findViewById(R.id.btnAgregar);
        btnSalir = findViewById(R.id.btnSalir);
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        txtNombre = findViewById(R.id.txtNombre);
        txtApellido = findViewById(R.id.txtApellido);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject objeto = new JSONObject();
                String respuesta="";
                try {
                    objeto.put("email",txtEmail.getText().toString());
                    objeto.put("password", txtPassword.getText().toString());
                    objeto.put("name",txtNombre.getText().toString());
                    objeto.put("lastName",txtApellido.getText().toString());
                    objeto.put("birthdate","\"2019-10-01\"");
                    objeto.put("profilePhotoUrl","null");
                    signUpApi(getApplicationContext(),objeto);
                } catch (JSONException e) {
                   Log.e("error",e.getMessage());
                } catch (Exception e) {
                    Log.e("error",e.getMessage());
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void signUpApi (Context contexto,final JSONObject param) {
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constantes.url_signUp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                        if (response != null){
                            boolean rError = false;
                            JSONObject objRespuesta = null;
                            objRespuesta = new JSONObject(response);

                            rError = objRespuesta.getBoolean("error");
                            if (!rError){
                                System.exit(0);
                            }else {
                                Log.i("app","Error");
                                Toast.makeText(PantallaRegistro.this,"Error en el registro.", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Log.e("app","Error al cargar el JSON");
                        }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                // the POST parameters:
                try {
                    params.put("email",param.getString("email"));
                    params.put("password",param.getString("password"));
                    params.put("name",param.getString("name"));
                    params.put("lastName",param.getString("lastName"));
                    params.put("birthdate",param.getString("birthdate"));
                    params.put("profilePhotoUrl","null");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        Volley.newRequestQueue(contexto).add(postRequest);
    }
}
