package com.example.app;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.app.common.Constantes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ConsumoApi {
    private String resultado = "";
    private JSONObject respuesta;

    public ConsumoApi() {
        this.resultado ="LKL";
    }

    protected String Login(String vEmail, String password, Context contexto) {
            String Resultado="";
            JSONObject param = new JSONObject();
        try {
            param.put("email",vEmail);
            param.put("password",password);
            Resultado = this.logIn(contexto,param);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return Resultado;
    }

    protected String SignUp(Context contexto,JSONObject param) {
        this.signUpApi(contexto,param);
        Log.i("app",this.resultado);
        return this.resultado;
    }

    private void signUpApi (Context contexto,final JSONObject param) {
        StringRequest postRequest = new StringRequest(Request.Method.POST, Constantes.url_signUp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        setResultado(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                // the POST parameters:
                try {
                    params.put("email",param.getString("email"));
                    params.put("password",param.getString("password"));
                    params.put("name",param.getString("name"));
                    params.put("lastName",param.getString("lastName"));
                    params.put("birthdate",param.getString("birthdate"));
                    params.put("profilePhotoUrl","null");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        Volley.newRequestQueue(contexto).add(postRequest);
    }
    public String logIn(Context contexto,final JSONObject param) {

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constantes.url_login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("api",response);
                        setResultado(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                // the POST parameters:
                try {
                    params.put("email", param.getString("username"));
                    params.put("password", param.getString("password"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        Volley.newRequestQueue(contexto).add(postRequest);
        return resultado;
    }


    public JSONObject metodoGet(final Context contexto, String id_busqueda){

        RequestQueue cola = Volley.newRequestQueue(contexto);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Constantes.url_book+id_busqueda.trim(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        respuesta = response;
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(contexto,"Error en la conexion", Toast.LENGTH_LONG).show();
            }
        });
        cola.add(jsonObjectRequest);
        return respuesta;
    }
    private final void setResultado(String valor){
        Log.i("app","nuevo valor");
        this.resultado = valor;
        Log.i("app",this.resultado);
    }

}

