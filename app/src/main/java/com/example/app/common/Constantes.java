package com.example.app.common;

public class Constantes {
    public final static String url_api = "https://api-eb.globeak.com/";
    public final static String url_book = url_api+"books/";
    public final static String url_login = url_api+"sessions/";
    public final static String url_signUp = url_api+"users/";
}
