package com.example.app.modelo;

import java.util.ArrayList;

public class categorias {
    private int id;
    private String nombre;
    private ArrayList<libro> listaLibros;
    public categorias(int id, String nombre, ArrayList<libro> listaLibros) {
        this.id = id;
        this.nombre = nombre;
        this.listaLibros = listaLibros;
    }

    public ArrayList<libro> getListaLibros() {
        return listaLibros;
    }

    public void setListaLibros(ArrayList<libro> listaLibros) {
        this.listaLibros = listaLibros;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
