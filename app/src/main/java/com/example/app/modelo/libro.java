package com.example.app.modelo;

import java.util.Date;

public class libro {
    private int id;
    private String name;
    private String pdfUrl;
    private String imageUrl;
    private String description;
    private Date postDate;
    private String author;
    private boolean state;
    private String editorial;
    private Date createAt;
    private Date updateAt;
    private int categoryId;

    public libro(int id, String name, String pdfUrl, String imageUrl, String description, Date postDate, String author, boolean state, String editorial, Date createAt, Date updateAt, int categoryId) {
        this.id = id;
        this.name = name;
        this.pdfUrl = pdfUrl;
        this.imageUrl = imageUrl;
        this.description = description;
        this.postDate = postDate;
        this.author = author;
        this.state = state;
        this.editorial = editorial;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
