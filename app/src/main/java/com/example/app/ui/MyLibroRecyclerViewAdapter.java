package com.example.app.ui;

import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.app.R;
import com.example.app.modelo.LibrosInteractionListenner;
import com.example.app.modelo.libro;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


public class MyLibroRecyclerViewAdapter extends RecyclerView.Adapter<MyLibroRecyclerViewAdapter.ViewHolder> {

    private final List<libro> mValues;
    private final LibrosInteractionListenner mListener;
    private Bitmap loadedImage;
    public MyLibroRecyclerViewAdapter(List<libro> items, LibrosInteractionListenner listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_libro, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.txtTitulo.setText(mValues.get(position).getName());
        holder.txtAutor.setText(mValues.get(position).getAuthor());
        holder.txtDescripcion.setText(mValues.get(position).getDescription());
       try {
          new SetImage(holder,mValues.get(position).getImageUrl()).execute();
           // holder.imgLibro.setImageBitmap(downloadFile(mValues.get(position).getImageUrl()));
       }catch (Exception e){
            Log.e("errorIMG",e.toString());
       }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.abrirLibro(holder.mItem);
                }
            }
        });

    }
    private Bitmap downloadFile(String imageHttpAddress) {
        URL imageUrl = null;
        try {
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            loadedImage = BitmapFactory.decodeStream(conn.getInputStream());
        } catch (IOException e) {
            Toast.makeText(null, "Error cargando la imagen: "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return loadedImage;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView txtTitulo;
        public final TextView txtAutor;
        public final TextView txtDescripcion;
        public  final ImageView imgLibro;
        public libro mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtTitulo = (TextView) view.findViewById(R.id.txtTitulo);
            txtAutor = (TextView) view.findViewById(R.id.txtAutor);
            txtDescripcion =(TextView) view.findViewById(R.id.txtDescripcion);
            imgLibro = (ImageView) view.findViewById(R.id.imgLibro);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtDescripcion.getText() + "'";
        }
    }

    private class SetImage extends AsyncTask<Void, Void, Bitmap> {

        final String urlImage ;
        private ViewHolder holder;
        public SetImage (ViewHolder holder,String url){
            this.holder = holder;
            this.urlImage = url;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap image = downloadFile(urlImage);
            return image;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
           holder.imgLibro.setImageBitmap(bitmap);
        }

    private Bitmap downloadFile(String imageHttpAddress) {
        URL imageUrl = null;
        try {
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            loadedImage = BitmapFactory.decodeStream(conn.getInputStream());
        } catch (IOException e) {
            Toast.makeText(null, "Error cargando la imagen: "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return loadedImage;
    }

}
}

