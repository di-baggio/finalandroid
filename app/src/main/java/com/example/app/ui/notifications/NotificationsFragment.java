package com.example.app.ui.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.app.R;
import com.example.app.common.Constantes;
import com.example.app.pantallaPrincipalActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    private TextView txtNombre, txtApellido;
    private Button btnModificar;
    private String user_id;
    private String token;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        txtNombre = root.findViewById(R.id.txtNombreUser);
        txtApellido  = root.findViewById(R.id.txtApellidoUser);
        btnModificar = root.findViewById(R.id.btnModificar);

        txtNombre.setText("");
        txtApellido.setText("");
        Bundle bundle = getArguments();

        if (bundle != null) {
            String id_user = bundle.getString("id", "0");
            this.token = bundle.getString("token", "0");
            this.user_id = id_user;
            getUser(id_user);
        }
        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(!txtNombre.getText().toString().isEmpty() & !txtApellido.getText().toString().isEmpty()) {
                   putUser();
               }else{
                   Toast.makeText(getContext(),"No puede actualizar la informacion, debe llenar todos los datos",Toast.LENGTH_SHORT).show();
               }
            }
        });
        return root;
    }

    private void getUser(String id_user){

        StringRequest postRequest = new StringRequest(Request.Method.GET, Constantes.url_signUp+id_user.trim(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        boolean rError = false;
                        if (response != null) {
                            try {

                                JSONObject objRespuesta = new JSONObject(response);
                                rError = objRespuesta.getBoolean("error");

                                    if (!rError) {
                                        txtNombre.setText(objRespuesta.getJSONObject("data").getString("name"));
                                        txtApellido.setText(objRespuesta.getJSONObject("data").getString("lastName"));
                                    } else {
                                        Toast.makeText(getContext(), "Error al consultar usuario", Toast.LENGTH_LONG).show();
                                    }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        Volley.newRequestQueue(getContext()).add(postRequest);
    }
    private void putUser(){
        final String token = this.token;
        StringRequest postRequest = new StringRequest(Request.Method.PUT, Constantes.url_signUp+"me",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        boolean rError = false;
                        if (response != null) {
                            try {
                                JSONObject objRespuesta = new JSONObject(response);
                                rError = objRespuesta.getBoolean("error");
                                if (rError) {
                                    Toast.makeText(getContext(),"ocurrió un al actualizar",Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getContext(),"Se actualizó correctamente",Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                // the POST parameters:
                    params.put("name", txtNombre.getText().toString().trim());
                    params.put("lastName", txtApellido.getText().toString().trim());
                    params.put("profilePhotoUrl", "");
                    params.put("birthdate", "2020-06-12");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization","Bearer "+ token.trim());

                return params;
            }

        };
        Volley.newRequestQueue(getContext()).add(postRequest);


    }
}