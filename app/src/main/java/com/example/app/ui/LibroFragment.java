package com.example.app.ui;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.app.ConsumoApi;
import com.example.app.R;
import com.example.app.common.Constantes;
import com.example.app.modelo.LibrosInteractionListenner;
import com.example.app.modelo.libro;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LibroFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private LibrosInteractionListenner mListener;
    private List<libro> Libros;
    private MyLibroRecyclerViewAdapter adapterLibros;

    public LibroFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static LibroFragment newInstance(int columnCount) {
        LibroFragment fragment = new LibroFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_libro_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            Libros = new ArrayList<>();
            ConsumoApi cm = new ConsumoApi();

            metodoGet(getContext(),"");


            Libros.add(new libro(1,"La voragine","http://kimera.com/data/redlocal/ver_demos/RLE/RECURSOS/REFERENCIA%20ESCOLAR/LITERATURA%20UNIVERSAL/COLOMBIANA/La%20vor%D0%B0gine/brblaa619043.pdf","https://www.libreriauca.com/system/balloom/product_assets/attachments/000/013/695/normal/LIB-9789996152719.jpg?1535685150", "La novela es una obra de denuncia social sobre la violencia y la situación de explotación que se vivió en la selva amazónica como consecuencia de la fiebre del caucho entre finales del siglo XIX e inicios del siglo XX.", new Date("02/12/2019"), "JOSÉ EUSTASIO RIVERA", true, "merca", new Date("02/12/2019"), new Date("02/12/2019"), 1));

            adapterLibros = new MyLibroRecyclerViewAdapter(Libros, mListener);
            recyclerView.setAdapter(adapterLibros);
        }
        return view;
    }
    private void metodoGet(final Context contexto, String id_busqueda){

        RequestQueue cola = Volley.newRequestQueue(contexto);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Constantes.url_book+id_busqueda.trim(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonRes =null;
                        JSONObject resultado = response;;

                        try {
                            jsonRes = resultado.getJSONArray("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            resultado = null;
                            jsonRes = null;
                        }

                        if (jsonRes !=null) {
                            for(int i=0;i<jsonRes.length();i++){
                                try {
                                    resultado = (JSONObject) jsonRes.get(i);
                                    Libros.add(new libro(resultado.getInt("id"),
                                            resultado.getString("name"),
                                            resultado.getString("pdfUrl"),
                                            resultado.getString("imageUrl"),
                                            resultado.getString("description"),
                                            null,
                                            resultado.getString("author"),
                                            resultado.getBoolean("state"),
                                            resultado.getString("editorial"),
                                            null,
                                            null,
                                            1));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(contexto,"Error en la conexion", Toast.LENGTH_LONG).show();
            }
        });
        cola.add(jsonObjectRequest);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LibrosInteractionListenner) {
            mListener = (LibrosInteractionListenner) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LibrosInteractionListenner");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
