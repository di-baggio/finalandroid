package com.example.app;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.app.common.Constantes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity  {
    Button btnLogin;
    Button btnRegistro;
    EditText etEmail;
    EditText etPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionbar = getSupportActionBar();
        actionbar.hide();

        btnLogin = findViewById(R.id.btnLogin);
        btnRegistro = findViewById(R.id.btnRegistro);
        etEmail  = findViewById(R.id.editTextEmail);
        etPassword  = findViewById(R.id.editTextPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (etEmail.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()) {
                    if (etEmail.getText().toString().isEmpty()) {
                        etEmail.setError("Este campo es obligatorio");
                    }
                    if (etPassword.getText().toString().isEmpty()) {
                        etPassword.setError("Este campo es obligatorio");
                    }
                }
                if (!etEmail.getText().toString().isEmpty() && !etPassword.getText().toString().isEmpty()) {

                   JSONObject param = new JSONObject();
                    try {
                        param.put("email",etEmail.getText().toString());
                        param.put("password",etPassword.getText().toString());
                        logIn(param);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

        });
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent pantallaRegistro = new Intent(v.getContext(), PantallaRegistro.class);
                pantallaRegistro.putExtra("username", etEmail.getText().toString());
                startActivity(pantallaRegistro);
            }
        });


    }
    private void logIn(final JSONObject param) {

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constantes.url_login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        boolean rError = false;
                        if (response != null) {
                            try {

                                JSONObject objRespuesta = new JSONObject(response);
                                rError = objRespuesta.getBoolean("error");
                                if (objRespuesta.getJSONObject("user").has("error")) {
                                } else {
                                    if (!rError) {
                                        Intent pantallaPrincipal = new Intent(getApplicationContext(), pantallaPrincipalActivity.class);
                                        Log.i("app",objRespuesta.getJSONObject("user").toString());
                                        pantallaPrincipal.putExtra("token",objRespuesta.getJSONObject("user").getString("token"));
                                        pantallaPrincipal.putExtra("id",objRespuesta.getJSONObject("user").getJSONObject("user").getString("id"));
                                        etEmail.setText("");
                                        etPassword.setText("");
                                        startActivity(pantallaPrincipal);

                                    } else {
                                        Log.i("app", "error");
                                        etPassword.setText("");
                                        etPassword.setError("Campo invalido");
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                // the POST parameters:
                try {
                    params.put("email", param.getString("email"));
                    params.put("password", param.getString("password"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(postRequest);
    }



}
