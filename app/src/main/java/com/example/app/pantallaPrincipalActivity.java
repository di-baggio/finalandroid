package com.example.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.app.modelo.LibrosInteractionListenner;
import com.example.app.modelo.libro;
import com.example.app.ui.LibroFragment;
import com.example.app.ui.dashboard.DashboardFragment;
import com.example.app.ui.notifications.NotificationsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class pantallaPrincipalActivity extends AppCompatActivity implements LibrosInteractionListenner {
    private String token,
                   id_user;

    BottomNavigationView navView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_principal);
        navView = findViewById(R.id.nav_view);


        navView.setOnNavigationItemSelectedListener(navListenner);
        try {
            getSupportFragmentManager().beginTransaction().add(R.id.contenedor,new LibroFragment()).commit();
            Bundle parametros = this.getIntent().getExtras();
            this.token = parametros.getString("token");
            this.id_user = parametros.getString("id");
        }catch (Exception e){

        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListenner =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment f = null;

                    switch (menuItem.getItemId()){
                        case R.id.navigation_home:
                            f = new LibroFragment();
                            break;
                        case R.id.navigation_dashboard:
                            f = new DashboardFragment();
                            break;
                        case R.id.navigation_notifications:
                            Bundle bundle = new Bundle();
                            bundle.putString("id",id_user);
                            bundle.putString("token",token);
                            f = new NotificationsFragment();
                            f.setArguments(bundle);

                            break;
                    }
                if (f != null){
                    getSupportFragmentManager().beginTransaction().replace(R.id.contenedor,f).commit();
                    return true;
                }
                return false;
                }
            };

    @Override
    public void abrirLibro(libro Libro) {

        Log.i("app","Has hecho click ");
        Intent intent = new Intent(getApplicationContext(),activity_viewerPDF.class);
        intent.putExtra("pdfUrl",Libro.getPdfUrl());
        startActivity(intent);
    }
}
